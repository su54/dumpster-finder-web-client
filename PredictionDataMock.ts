export const PredictionDataMock = [
  {x1: 25, y1: 25, x2: 300, y2: 300, class_name: 'full', detect_prob: 0.91, classify_prob: 0.86},
  {x1: 250, y1: 500, x2: 50, y2: 50, class_name: 'empty', detect_prob: 0.91, classify_prob: 0.86},
  {x1: 300, y1: 300, x2: 400, y2: 400, class_name: 'halfempty', detect_prob: 0.91, classify_prob: 0.86}
];

/*
[

[1527, 686, 1898, 1151, 'undefined', 0.91, 0.86],

[722, 646, 1094, 1116, 'full', 0.89, 0.84],

[2462, 726, 2781, 1133, 'full', 0.87, 0.96],

[2159, 727, 2479, 1133, 'full', 0.78, 0.96],

[1078, 575, 1484, 1125, 'full', 0.7, 0.76],

[1872, 700, 2179, 1138, 'full', 0.46, 0.94],

[359, 565, 758, 1090, 'undefined', 0.32, 0.93]

]
*/

/*Questions:
* 1) General design
* 2) Multi coloring (based on detection prob)
* 3) Delivery (docker image + environment variables (addresses))
* 4) Server response parsing (is it final)
* 5) Server url option
* 6) Easy tasks (3) *Ada
* */
