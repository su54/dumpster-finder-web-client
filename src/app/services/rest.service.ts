import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class RestService {
  private http: HttpClient;
  private backUrl: string;
  private httpOptions: {headers: HttpHeaders};

  constructor(http: HttpClient) {
    this.http = http;
    this.backUrl = "/predictions/garbage_cont_classify";
  }

  sendImage(image: any, serverAddress: string): Observable<any> {
    this.httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'image/png'})
    }
    let formData: FormData = new FormData();
    formData.append(
      'data',
      new Blob([image.src], {type: 'image/png'}),
      'picture.png'
    );
    return this.http.post(
      serverAddress + this.backUrl,
      formData,
      this.httpOptions
    );
  }
}
