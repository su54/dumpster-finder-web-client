import {Component, ElementRef, ViewChild} from '@angular/core';
import {RestService} from "./services/rest.service";
import {PredictionDataMock} from "../../PredictionDataMock";
import {Dto} from "./payload/dto";
import {ServerAddressConfig} from "../../ServerAddressConfig";
import {FormControl, FormGroup} from "@angular/forms";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public imageWidth: number;
  public imageHeight: number;
  public image: any;
  public predictionData: any[];
  public isImageSelected: boolean;
  public layerCanvasElement: any;
  public addressList: string[];
  @ViewChild("canvasLayer", { static: false })
  private layerCanvas: ElementRef;
  private context: CanvasRenderingContext2D;
  private restClient: RestService;

  addressForm: FormGroup = new FormGroup({
      address: new FormControl('')
  });

  constructor(restClient: RestService) {
    this.restClient = restClient;
    this.isImageSelected = false;
    this.predictionData = PredictionDataMock;
    this.addressList = ServerAddressConfig;
  }

  onSelectFile(event: any): void {
    let reader = new FileReader();
    reader.readAsDataURL(event.target.files[0]);
    reader.onload = () => {
      this.image = new Image();
      this.image.src = reader.result;
      this.image.onload = () => {
        this.imageWidth = this.image.width;
        this.imageHeight = this.image.height;
        this.isImageSelected = true;
      }
    }
  }

  showImage(): void {
    this.layerCanvasElement = this.layerCanvas.nativeElement;
    this.context = this.layerCanvasElement.getContext("2d");
    this.layerCanvasElement.width = this.imageWidth;
    this.layerCanvasElement.height = this.imageHeight;
    this.context.drawImage(this.image, 0, 0, this.imageWidth, this.imageHeight);
    this.requestPredictionDataOnImage(this.image);
    this.drawRectangles();
  }

  drawRectangles(): void {
    for (let j = 0; j < this.predictionData.length; j++) {
      let x1 = this.predictionData[j].x1;
      let y1 = this.predictionData[j].y1;
      let x2 = this.predictionData[j].x2;
      let y2 = this.predictionData[j].y2;
      this.context.rect(x1, y1, x2, y2);
    }
    this.context.strokeStyle="RED";
    this.context.lineWidth = 3;
    this.context.stroke();
  }

  private requestPredictionDataOnImage(image: any): void {
    let targetAddress = this.addressForm.controls['address'].value;
    if (targetAddress == '') {
      targetAddress = ServerAddressConfig[0];
    }
    this.restClient.sendImage(image, targetAddress).subscribe(
      data => {
        if (data.length != 0) {
          this.predictionData = this.parsePythonArray(data);
        }
      },
      error => {
        alert('Enable to reach chosen server, using mock');
      }
    );
  }

  //to do...
  private parsePythonArray(data: string): Dto[] {
    this.removeCharAt(data, 0);
    this.removeCharAt(data, data.length);
    let dtoList: Array<Dto> = new Array<Dto>();
    let dtoStringList: string[] = data.split(',\n');
    for (let predict in dtoStringList) {
      dtoList.push(this.getDtoFromString(predict));
    }
    return dtoList;
  }

  private getDtoFromString(string: string): Dto {
    this.removeCharAt(string, 0);
    this.removeCharAt(string, string.length);
    let values: string[] = string.split(',');
    let dto = {
      x1: parseInt(values[0]),
      y1: parseInt(values[1]),
      x2: parseInt(values[2]),
      y2: parseInt(values[3]),
      className: values[4],
      detectProb: parseFloat(values[5]),
      classifyProb: parseFloat(values[5])
    };
    return <Dto> dto;
  }

  private removeCharAt(string: string, index: number) {
    let tmp = string.split('');
    tmp.splice(index, 1);
    return tmp.join('')
  }
}
